extern crate derive_builder;
pub mod parser;

use std::{fs, io};
use clap::Parser;
use quick_xml::events::Event;
use quick_xml::Reader;
use anyhow::Result as AnyhowResult;
use protosdc_mappers::mappers::rust_to_proto::map_protosdc_biceps_biceps_getmdibresponse;
use prost::Message;
use protosdc_biceps::biceps::GetMdibResponse;

//noinspection HttpUrlsUsage
const MESSAGE_MODEL: &[u8] = b"http://standards.ieee.org/downloads/11073/11073-10207-2017/message";

#[derive(Parser, Debug)]
#[clap(author, version, about, long_about = None)]
struct Args {
    /// Name of the input mdib
    #[clap(short, long)]
    input: String,

    /// Number of the output mdib
    #[clap(short, long, default_value_t = String::from("out.protosdc"))]
    output: String,
}

fn main() {
    let args = Args::parse();

    println!("From {} to {}", args.input, args.output);

    parse_get_mdib(args.input, args.output).unwrap();
}

fn read_file(path: &str) -> Result<Vec<u8>, io::Error> {
    let file = fs::read(path)?;
    Ok(file)
}

fn parse_get_mdib(input: String, output: String) -> AnyhowResult<()> {

    let content = read_file(&input)?;

    let mut reader = Reader::from_reader(content.as_slice());
    reader.trim_text(true);
    reader.expand_empty_elements(true);
    let mut ns_buf: Vec<u8> = vec![];

    let mut buf = vec![];

    let get_mdib_response: GetMdibResponse = loop {
        match reader.read_namespaced_event(&mut buf, &mut ns_buf) {
            Ok((ref ns, Event::Start(ref e))) => {
                break parser::parse_get_mdib_response_complex((Some(MESSAGE_MODEL), b"GetMdibResponse"), e, &mut reader, &mut ns_buf)?;
            },
            Ok((ref ns, Event::DocType(ref e))) => {},
            Ok((ref ns, Event::Decl(ref e))) => {},
            Ok((ref ns, Event::Comment(ref e))) => {},
            _ => panic!("Could not parse mdib, unexpected initial event")
        };
    };

    let mapped = map_protosdc_biceps_biceps_getmdibresponse(get_mdib_response);

    let mapped_bytes = mapped.encode_to_vec();

    fs::write(output, &mapped_bytes)?;

    Ok(())
}